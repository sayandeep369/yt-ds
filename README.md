# yt-ds
A script for searching youtube videos though terminal(with the help of youtube-search-python) and downloading them with the help of youtube-dl.

## Requirement:

1)Python3 or above(https://www.python.org)

2)pip(https://pip.pypa.io/en/stable/quickstart/)

## Running the script:

1) Start a new terminal session.
2) Clone this repository:
  ```sh
   git clone https://github.com/IBR-41379/yt-ds ~/yt-ds
  ```
3)Go to the directory:
  ```sh
   cd ~/
   cd yt-ds
  ```
4)Launch the script:
  ```sh
   python3 yt-ds.py
  ```
